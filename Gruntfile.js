// Generated on 2013-09-06 using generator-angular 0.4.0
'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({ port: LIVERELOAD_PORT });
var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);
  grunt.loadNpmTasks('grunt-angular-templates');

  // configurable paths
  var yeomanConfig = {
    app: 'app',
    dist: 'dist'
  };

  try {
    yeomanConfig.app = require('./bower.json').appPath || yeomanConfig.app;
  } catch (e) {}

  grunt.initConfig({
    yeoman: yeomanConfig,
    watch: {
      coffee: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.coffee'],
        tasks: ['coffee:dist']
      },
      coffeeTest: {
        files: ['test/spec/{,*/}*.coffee'],
        tasks: ['coffee:test']
      },
      styles: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css'],
        tasks: ['copy:styles', 'autoprefixer']
      },
      livereload: {
        options: {
          livereload: LIVERELOAD_PORT
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '{.tmp,<%= yeoman.app %>}/scripts/{,*/}*.js',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },
    open: {
      server: {
        url: 'http://localhost:<%= connect.options.port %>'
      }
    },
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [
          '<%= yeoman.app %>/scripts/*.js',
          '<%= yeoman.app %>/scripts/services/*.js',
          '<%= yeoman.app %>/scripts/directives/*.js',
          '.tmp/templates.js'
        ],
        dest: 'dist/lysander.shell.js',
      },
    },
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>/',
          src: '*.js',
          dest: '<%= yeoman.dist %>/'
        }]
      }
    },
    uglify: {
      dist: {
        files: {
          '<%= yeoman.dist %>/lysander.shell.js' : [ '<%= yeoman.dist %>/lysander.shell.js' ]
        }
      }
    },
    ngtemplates: {
      "default" : {
        options: {
          base:       'app/views',              // $templateCache ID will be relative to this folder
          prepend:    'lysander.shell/',        // (Optional) Prepend path to $templateCache ID
          module:     'lysander.shellApp',      // (Optional) The module the templates will be added to
          htmlmin: {
            collapseWhitespace: true
          }
        },
        src:          'app/views/**.html',
        dest:         '.tmp/templates.js'
      }
    }

  });

  grunt.registerTask('build', [
    'clean:dist',
    'ngtemplates',
    'concat',
    'ngmin',
    'uglify',
  ]);

  grunt.registerTask('default', [
    'build'
  ]);
};
