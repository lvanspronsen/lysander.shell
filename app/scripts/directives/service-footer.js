'use strict';

angular.module('lysander.shellApp')
  .directive('serviceFooter', function () {
    return {
      templateUrl : 'lysander.shell/service-footer.html',
      restrict: 'E',
      scope : { service : '=' }
    };
  });
