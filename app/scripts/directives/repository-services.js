'use strict';

angular.module('lysander.shellApp')
  .directive('repositoryServices', function (Service) {
    return {
      templateUrl: 'lysander.shell/repository-services.html',
      restrict: 'E',
      scope : { repo : '@' },
      controller : function($rootScope, $scope) {

        $scope.schema = null;

      	$scope.$watch('repo', function() {

          $scope.find = new Service('POST', $scope.repo + '/find', true);
          $scope.all = new Service('POST', $scope.repo + '/all', true);
          $scope.query = new Service('POST', $scope.repo + '/query', true);
          $scope.first = new Service('POST', $scope.repo + '/first', true);
          $scope.insert = new Service('POST', $scope.repo + '/insert', true);
          $scope.update = new Service('POST', $scope.repo + '/update', true);
          $scope.del = new Service('POST', $scope.repo + '/delete', true);

          var insertFields = $scope.insertFields = [{}];
          var updateFields= $scope.updateFields = [{}];

          $scope.$watch('insertFields', function() {
            var obj = { };
            for(var i = 0; i < insertFields.length; i++) {
              var field = insertFields[i];
              obj[field.field] = field.value;
            }
            $scope.insert.data = obj;
          }, true);

          $scope.$watch('updateFields', function() {
            var obj = { id : $scope.update.data.id };
            for(var i = 0; i < updateFields.length; i++) {
              var field = updateFields[i];
              obj[field.field] = field.value;
            }
            $scope.update.data = obj;
          }, true);

          $scope.insertFieldChanged = function(index, field) {
            if(!field.field && !field.value) {
              insertFields.splice(index, 1);
            } else if(index == insertFields.length - 1) {
              insertFields.push({});
            }
          };

          $scope.updateFieldChanged = function(index, field) {
            if(!field.field && !field.value) {
              updateFields.splice(index, 1);
            } else if(index == updateFields.length - 1) {
              updateFields.push({});
            }
          };

      	});


        $rootScope.$watch('token', function(token) {
          if(token) {
            var service = new Service('GET', $scope.repo + '/schema', true);
            service.on('success', function(res) {
              $scope.schema = res;
            });
            service.send();
          }
        });
      }
    };
  });
