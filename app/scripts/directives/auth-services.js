'use strict';

angular.module('lysander.shellApp')
  .directive('authServices', function () {
    return {
      templateUrl: 'lysander.shell/auth-services.html',
      restrict: 'E',
      controller : function($scope, $rootScope, Service) {

      	$scope.authenticate = new Service('POST', 'auth/authenticate', false);
      	$scope.restore = new Service('POST', 'auth/restore', false);
      	$scope.revoke = new Service('POST', 'auth/revoke', false);

      	$scope.authenticate.on('success', function(token) {
      		if(token) {
      			$rootScope.token = token;
      		}
      	});
      }
    };
  });
