'use strict';

angular.module('lysander.shellApp')
  .directive('queryBuilder', function () {
    return {
      templateUrl: 'lysander.shell/query-builder.html',
      restrict: 'E',
      scope : { schema:'=', query : '=' },
      controller : function($scope) {

      	var filters = $scope.filters = [{}];
      	var orders = $scope.orders = [{ dir : 'asc' }];
      	var fields = $scope.fields = [{}];

      	// transfer temporary arrays to query object

      	$scope.$watch('filters', function(arr) {
      		$scope.query.data.filters = _.first(arr, arr.length - 1);
      	}, true);

      	$scope.$watch('orders', function(arr) {
      		$scope.query.data.orderBy = _.first(arr, arr.length - 1);
      	}, true);

      	$scope.$watch('fields', function(arr) {
      		$scope.query.data.fields = _.pluck(_.first(arr, arr.length - 1), 'field');
      	}, true);

      	// generate and remove entries on edit

      	$scope.filterChanged = function(index, filter) {
      		if(!filter.field && !filter.value) {
      			filters.splice(index, 1);
      		} else if(index == filters.length - 1) {
      			filters.push({});
      		}
      	};

      	$scope.orderChanged = function(index, order) {
      		if(!order.field) {
      			orders.splice(index, 1);
      		} else if(index == orders.length - 1 ) {
      			orders.push({ dir : 'asc' });
      		}
      	};

      	$scope.fieldChanged = function(index, field) {
      		if(!field.field) {
      			fields.splice(index, 1);
      		} else if(index == fields.length -1 ) {
      			fields.push({});
      		}
      	};

      }
    };
  });
